package org.bibsonomy.texlipseextension.model;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.bibsonomy.common.enums.SerializeBibtexMode;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.util.BibTexUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;



/**
 * Class that represents the bib entries of a file + the position (in lines) in
 * the file offers a method to delete the specific entry from the file. Here the
 * BibTex-key is unique, so there don't exist different entries with the same
 * key in the file
 * 
 * @author Manuel
 * 
 */
public class BibFileRepresentation {

	/** the bibTex Key and the position of the entry in the file */
	private Map<String, BibFileEntryData> entries;
	
	private final IFile ifile;

	/** contains the file content as String */
	private StringBuffer fileContent;
	
	public BibFileRepresentation(final IFile ifile) {
		super();
		this.ifile = ifile;
		this.build();
	}
	
	private void build() {
		this.entries = new HashMap<String, BibFileEntryData>();
		this.fileContent = new StringBuffer();
		try {
			final BufferedReader reader = new BufferedReader(new InputStreamReader(this.ifile.getContents(), this.ifile.getCharset()));
			String currentEntry = "";
			String currentLine;
			// pattern to check if a new entry begins
			final Pattern p = Pattern.compile("\\s*@.*\\{.*,");
			final Pattern p2 = Pattern.compile("\\}.*@.*\\{.*,");
			
			int counter = 0;
			int start = 0;
			StringBuffer data = new StringBuffer();
			while ((currentLine = reader.readLine()) != null) {		
				this.fileContent.append(currentLine + System.getProperty("line.separator"));
				data.append(currentLine + System.getProperty("line.separator"));
				if (currentLine.matches(p.pattern())) {
					currentEntry = currentLine.split("\\{")[1];
					currentEntry = currentEntry.split(",")[0];
					start = counter++;
				}
				//here is a problem with the closing "}"-char from entry 1 and the start 
				//"@"-char from entry 2 in the same line. So we catch it with p2
				else if (currentLine.matches(p2.pattern())){
					final String modifiedData = data.toString().replace(currentLine, "}")+ System.getProperty("line.separator");
					try {
						this.entries.put(currentEntry, new BibFileEntryData(modifiedData, start, counter++));
					} catch (final bibtex.parser.ParseException e) {
						//e.printStackTrace();
					}
					data = new StringBuffer();
					currentLine = currentLine.replaceFirst("\\}", "");
					data.append(currentLine + System.getProperty("line.separator"));
					currentEntry = currentLine;
					currentEntry = currentEntry.split("\\{")[1];
					currentEntry = currentEntry.split(",")[0];
					start = counter - 1;
				} else if (currentLine.matches("\\s*}")) {
					BibFileEntryData entryData;
					try {
						entryData = new BibFileEntryData(data.toString(), start, counter++);
						this.entries.put(currentEntry,entryData);
					} catch (final bibtex.parser.ParseException e) {
						//e.printStackTrace();
					}		
					data = new StringBuffer();
				} else {
					counter++;
				}
			}
			reader.close();
		} catch (final CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public IFile getFile() {
		return this.ifile;
	}
	
	/**
	 * Deletes a set of entries from the file, returns the a list with the keys
	 * of the entries that are deleted.
	 * 
	 * @param keys
	 *            the keys of the entries to delete
	 * @return the keys that have been deleted
	 */
	private void deleteEntriesFromFile(final List<String> bibs) {
		try {
			final StringBuilder builder = new StringBuilder();
			for (final String k : this.entries.keySet()) {
				if (!bibs.contains(k)) {
					builder.append(BibTexUtils.toBibtexString(this.entries.get(k).getBibTex()));
					builder.append(System.getProperty("line.separator"));
				}
			}
			this.ifile.setContents(new ByteArrayInputStream(builder.toString().getBytes("UTF-8")), IFile.FORCE, null);
		} catch (final CoreException e) {
			//e.printStackTrace();
		} catch (final UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Map<String, BibFileEntryData> getEntries() {
		this.build();
		return this.entries;
	}

	/**
	 * Fetches the Bib-entry for a bibTexKey as String
	 * 
	 * @param bibTexKey
	 * @return
	 */
	public String getBibData(final String bibTexKey) throws NullPointerException {
		this.build();
		final String back = BibTexUtils.toBibtexString(this.entries.get(bibTexKey).getBibTex());
		if (back != null) {
			return BibTexUtils.toBibtexString(this.entries.get(bibTexKey).getBibTex());
		} else {
//			String message = "The BibTex entry with the key "+bibTexKey + "doesn't exist in the file "+ getFile().getAbsolutePath();
			//TexlipsePlugin.log(message, new NullPointerException());
			return "";
		}
	}

	/**
	 * Refreshes the given entries in the bibTex file
	 * 
	 * @param bibTexEntries
	 *            the entries that shall be updated, the structure of the map is
	 *            <bib-key, bib-entry>
	 */
	public boolean updateEntriesInFile(final List<BibTex> bibTexEntries) {
		this.build();
		final List<String> keysToDelete = new ArrayList<String>();
		for (final BibTex bib : bibTexEntries) {
			keysToDelete.add(bib.getBibtexKey());
		}
		this.deleteEntriesFromFile(keysToDelete);
		return this.writeEntriesToFile(bibTexEntries);
	}

	/**
	 * Writes some bibTex entries in the file, there is no check if the entry is
	 * already written in the file so there can be duplicates. Caller must
	 * secure that there are no duplicates
	 * 
	 * @param bibTexEntries
	 *            a map with bibTex keys as keys and bibTex entry as value
	 * @return true if success
	 */
	private synchronized boolean writeEntriesToFile(final List<BibTex> bibTexEntries) {
		try {
			final StringBuilder buf = new StringBuilder();
			for (final BibTex bib : bibTexEntries) {		
				final String data = BibTexUtils.toBibtexString(bib,SerializeBibtexMode.PARSED_MISCFIELDS);
				buf.append(data);
				if (!data.endsWith(System.getProperty("line.separator"))) {
					buf.append(System.getProperty("line.separator"));
				}
			}
			
			this.ifile.appendContents(new ByteArrayInputStream(buf.toString().getBytes("UTF-8")), IFile.FORCE, null);
			return true;
		} catch (final IOException e) {
			e.printStackTrace();
		} catch (final CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Writes a single bibTex entry to the current file.
	 * There is a check whether this entry already exists in the file. If it's in the file 
	 * there will be no write action.
	 * 
	 * @param bib
	 *            the BibTex entry
	 * @return true if success
	 */
	public boolean writeEntryToFile(final BibTex bib) {
		final List<BibTex> toWrite = new ArrayList<BibTex>();
		// check if the file contains this entry
		if (!this.entries.keySet().contains(bib.getBibtexKey())) {
			toWrite.add(bib);
			return this.writeEntriesToFile(toWrite);
		}
		
		return false;		
	}
	
//	/**
//	 * update an old, or add  a new entry to the file.
//	 * @param key key of the entry to add
//	 * @param data data of the entry
//	 * @return
//	 */
//	public boolean updateEntryInFile(String data) {
//		//ToDo: check if data is a valid bibtexentry
//		BibFileEntryData bib = null;
//		try {
//			bib = new BibFileEntryData(data, 0, 0);
//		} catch (bibtex.parser.ParseException e) {
//			return false;
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return false;
//		}
//		if (bib.getBibTex() == null) return false;
//		if (entries.containsKey(bib.getBibTex().getBibtexKey())) {
//			this.deleteEntryFromFile(bib.getBibTex().getBibtexKey());
//			entries.remove(bib.getBibTex().getBibtexKey());
//			this.writeEntryToFile(bib.getBibTex());
//			this.build(getFile());
//			return true;
//		}
//		else {
//			this.writeEntryToFile(bib.getBibTex());
//			this.build(getFile());
//			return true;
//		}
//	}
	
}
