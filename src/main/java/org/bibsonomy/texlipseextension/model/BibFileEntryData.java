package org.bibsonomy.texlipseextension.model;

import java.io.IOException;

import org.bibsonomy.bibtex.parser.PostBibTeXParser;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.util.SimHash;

import bibtex.parser.ParseException;

/**
 * Class that represents an BibTex entry in a File
 * 
 * @author Manuel
 *
 */
public class BibFileEntryData {

	/** the position of the entry in the file (line numbers)	 */
	private final int[] position;
	/** the content of the entry as {@link BibTex}	 */
	private final BibTex bibData;

	/**
	 * Constructs a new BibTex entry
	 * @param data the data of the entry as String
	 * @param start the line in the file where the entry starts
	 * @param end the line in the file where the entry ends
	 * @throws ParseException
	 * @throws IOException
	 */
	public BibFileEntryData(final String data, final int start, final int end) throws ParseException, IOException {	
		final PostBibTeXParser p = new PostBibTeXParser();
		// FIXME: Postbibtexparser parst falsch! und was?!
		final BibTex bib = p.parseBibTeX(data);
		this.bibData = bib;
		this.position = new int[2];
		this.position[0] = start;
		this.position[1] = end;
	}

	/**
	 * Creates the BibSonomy hash for the entry
	 * @return
	 */
	public String getHash() {
		String hash = this.bibData.getMiscField("intrahash");
		if (hash == null) {
			hash = SimHash.getSimHash2(this.bibData);
		}
		return hash;
	}

	public int getStartPosition() {
		return this.position[0];
	}
	
	public int getEndPosition() {
		return this.position[1];
	}
	
	public int[] getPosition() {
		return this.position;
	}
	
	public void setPosition(final int start, final int end) {
		this.position[0] = start;
		this.position[1] = end;
	}
	
	public BibTex getBibTex() {
		return this.bibData;
	}
	
//	/**
//	 * The BibTex entry as String.
//	 * 
//	 * @return The BibTex entry as String
//	 */
//	public String getBibString() {
//		return BibTexUtils.toBibtexString(bibData);
//	}
	
//	@Override
//	public boolean equals(Object other) {
//		if (this == other)
//		       return true; 
//		if (other == null)
//		       return false; 
//		if (other.getClass() != getClass())
//		      return false; 
//		return this.getBibString().equals(((BibFileEntryData)other).getBibString());
//	}
	
}
