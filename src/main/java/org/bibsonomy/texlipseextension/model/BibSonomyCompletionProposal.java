package org.bibsonomy.texlipseextension.model;

import java.util.ArrayList;
import java.util.List;

import net.sourceforge.texlipse.TexlipsePlugin;
import net.sourceforge.texlipse.model.ReferenceContainer;
import net.sourceforge.texlipse.model.ReferenceEntry;
import net.sourceforge.texlipse.properties.TexlipseProperties;

import org.bibsonomy.model.Post;
import org.bibsonomy.texlipseextension.Bibsonomyconnection.BibSonomyConnector;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

/**
 * This class is similar to {@link CompletionProposal} but differs in the "apply"-
 * method. When a proposal is selected in the \cite auto-completion it will
 * write its content in the first file that is specified in the \bibliography
 * command.
 * 
 * @author Manuel
 * 
 */
public class BibSonomyCompletionProposal implements ICompletionProposal {

	private final int fReplacementOffset;

	private int fCursorPosition;

	private final Image fImage;

	private final String fDisplayString;

	private final IContextInformation fContextInformation;

	private final String fAdditionalProposalInfo;

	private int fReplacementLength;

	private final ReferenceEntry entry;

	private final ReferenceContainer bibContainer;

	public BibSonomyCompletionProposal(final int offset, final Image fImage,
			final String fDisplayString, final IContextInformation fContextInformation,
			final String fAdditionalProposalInfo, final int fReplacementLength,
			final ReferenceEntry entry, final ReferenceContainer bibContainer) {
		super();
		this.fReplacementLength = fReplacementLength;
		this.fReplacementOffset = offset - fReplacementLength;
		this.fImage = fImage;
		this.fDisplayString = fDisplayString;
		this.fContextInformation = fContextInformation;
		this.fAdditionalProposalInfo = fAdditionalProposalInfo;
		this.fReplacementLength = fReplacementLength;
		this.entry = entry;
		this.bibContainer = bibContainer;
	}

	@Override
	public Point getSelection(final IDocument document) {
		return new Point(this.fReplacementOffset + this.fCursorPosition, 0);
	}

	@Override
	public Image getImage() {
		return this.fImage;
	}

	@Override
	public String getDisplayString() {
		return this.fDisplayString;
	}

	@Override
	public IContextInformation getContextInformation() {
		return this.fContextInformation;
	}

	@Override
	public String getAdditionalProposalInfo() {
		return this.fAdditionalProposalInfo;
	}

	@Override
	public void apply(final IDocument document) {
		try {
			document.replace(this.fReplacementOffset, this.fReplacementLength, this.entry.key);
			final BibSonomyConnector bibSonomyConnector = BibSonomyConnector.getInstance();
			final List<Post<?>> matchingPosts = BibSonomyConnector.getInstance().getHashesForKey(this.fDisplayString);
			if (bibSonomyConnector.saveToFile(matchingPosts.get(0).getResource().getIntraHash(), TexlipsePlugin.getCurrentProject())) {
				final List<ReferenceEntry> entries = this.bibContainer.getSortedReferences();
				final List<ReferenceEntry> toAdd = new ArrayList<ReferenceEntry>();			
				for(final ReferenceEntry ref : entries) {
					if (ref.fileName.equals(ref.fileName)) {
						toAdd.add(ref);
					}
				}
				
				final String[] bibFiles = (String[]) TexlipseProperties.getSessionProperty(
						TexlipsePlugin.getCurrentProject(),
						TexlipseProperties.BIBFILE_PROPERTY);
				String refSource ="BibSonomy";
				if (bibFiles.length >0) {
					refSource = bibFiles[0];
				}
				
				toAdd.add(this.entry);
				this.bibContainer.updateRefSource(refSource, toAdd);
				this.bibContainer.organize();
			}

		} catch (final org.eclipse.jface.text.BadLocationException e) {
			// e.printStackTrace();
		}
	}
}
