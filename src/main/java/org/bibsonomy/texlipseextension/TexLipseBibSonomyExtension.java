package org.bibsonomy.texlipseextension;

import net.sourceforge.texlipse.TexlipsePlugin;

import org.bibsonomy.texlipseextension.Bibsonomyconnection.BibSonomyConnector;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 * 
 * Some methods are directly copied from the {@link TexlipsePlugin} class, this 
 * is an artifact from the time when this plug-in was directly integrated in TexLipse
 */
public class TexLipseBibSonomyExtension extends AbstractUIPlugin {

	/** The plug-in ID */
	public static final String PLUGIN_ID = "org.bibsonomy.texlipseextension";

	private static final String ICONS_PATH = "icons/";
	
	/** key for storing and accessing the specified username */
	public static final String PREF_KEY_USERNAME = "bibSonomyUsername";

	/** key for storing and accessing the specified api key */
	public static final String PREF_KEY_APIKEY = "bibSonomyApiKey";

	/** key for storing and accessing the specified tags */
	public static final String PREF_KEY_TAGS = "bibSonomyTags";

	/** key for storing and accessing if plugin is enabled by the user */
	public static final String PREF_KEY_ENABLED = "useBibSonomy";
	
	/** key for storing the api url */
	public static final String PREF_KEY_API_URL = "BibSonomyAPIUrl";

	// The shared instance
	private static TexLipseBibSonomyExtension plugin;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		this.getPreferenceStore().addPropertyChangeListener(new IPropertyChangeListener() {
			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				final String property = event.getProperty();
				if (TexLipseBibSonomyExtension.PREF_KEY_TAGS.equals(property) || TexLipseBibSonomyExtension.PREF_KEY_ENABLED.equals(property)) {
					BibSonomyConnector.getInstance().refreshAllData();
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(final BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the singleton instance
	 * 
	 * @return the singleton instance
	 */
	public static TexLipseBibSonomyExtension getDefault() {
		return plugin;
	}

	/**
	 * Return an image from the plugin's icons-directory.
	 * 
	 * @param name
	 *            name of the icon
	 * @return the icon as an image object
	 */
	public static Image getImage(final String name) {
		return getDefault().getCachedImage(name);
	}
	
	public static IFile getFile(final IProject project, final String path) {
		return project.getFile(path);
	}

	/**
	 * Cache the image if it is found.
	 * 
	 * @param key
	 *            name of the image
	 * @return image from the cache or from disk, null if image is not found in
	 *         either
	 */
	protected Image getCachedImage(final String key) {
		if (key == null) {
			return null;
		}
		Image g = this.getImageRegistry().get(key);
		if (g != null) {
			return g;
		}
		g = getImageDescriptor(key).createImage();
		return g;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(final String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH + path + ".gif");
	}
}
