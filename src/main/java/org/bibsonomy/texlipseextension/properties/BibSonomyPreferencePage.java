package org.bibsonomy.texlipseextension.properties;


import org.bibsonomy.texlipseextension.TexLipseBibSonomyExtension;
import org.bibsonomy.texlipseextension.Bibsonomyconnection.BibSonomyConnector;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringButtonFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * This class represents the BibSonomy-TexLipse preference page. It is integrated directly in
 * the TexLipse Preferences.
 * 
 * @author Manuel
 *
 */
public class BibSonomyPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	
	/**
	 * default constructor
	 */
	public BibSonomyPreferencePage() {
		super(GRID);
		this.setPreferenceStore(TexLipseBibSonomyExtension.getDefault().getPreferenceStore());
		this.setDescription("Bibsonomy Settings");
	}

	/**
	 * Creates the field editors.
	 */
	@Override
	public void createFieldEditors() {
		this.addField(new BooleanFieldEditor(TexLipseBibSonomyExtension.PREF_KEY_ENABLED, "Enable BibSonomy import", this.getFieldEditorParent()));
		this.addField(new StringFieldEditor(TexLipseBibSonomyExtension.PREF_KEY_USERNAME, "User name:", this.getFieldEditorParent()));
		final StringButtonFieldEditor e = new StringButtonFieldEditor(TexLipseBibSonomyExtension.PREF_KEY_APIKEY, "API Key:", this.getFieldEditorParent()) {
			@Override
			protected String changePressed() {
				BibSonomyPreferencePage.this.performApply();
				BibSonomyConnector.getInstance().refreshAllData();
				if (!BibSonomyConnector.getInstance().checkUsernameApiKeyValid()) {
					MessageDialog.openWarning(this.getShell(), "Username - API key not valid", "Your username - API key combination isn't valid");
				}
				else {
					MessageDialog.openInformation(this.getShell(), "Username - API key not valid", "Your username - API key combination is valid");
				}
					return null;
			
			}
		};
		e.setChangeButtonText("Check");
		this.addField(e);
		this.addField(new StringFieldEditor(TexLipseBibSonomyExtension.PREF_KEY_API_URL, "API Url:", this.getFieldEditorParent()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(final IWorkbench workbench) {
		// noop
	}

}