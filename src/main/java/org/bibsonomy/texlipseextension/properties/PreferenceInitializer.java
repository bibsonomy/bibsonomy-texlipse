package org.bibsonomy.texlipseextension.properties;

import org.bibsonomy.texlipseextension.TexLipseBibSonomyExtension;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * inits the preferences
 *
 * @author dzo
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		final IPreferenceStore store = TexLipseBibSonomyExtension.getDefault().getPreferenceStore();
		store.setDefault(TexLipseBibSonomyExtension.PREF_KEY_API_URL, "http://www.bibsonomy.org/api/"); // TODO: after rest-client update replace with RestLogicFactory.API_URL
	}

}
