package org.bibsonomy.texlipseextension.actions;


import org.bibsonomy.texlipseextension.TexLipseBibSonomyExtension;
import org.bibsonomy.texlipseextension.dialog.TagDialog;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;

/**
 * ActionDelegate for the change-tags button on top of the latex editor
 * 
 * @author Manuel
 *
 */
public class TagsButtonAction implements IEditorActionDelegate {

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
	}

	public void run(IAction action) {
		IWorkbenchWindow window = TexLipseBibSonomyExtension.getDefault()
		.getWorkbench().getActiveWorkbenchWindow();
		//opens the TagDialog when the button is clicked
		TagDialog dia = new TagDialog(window.getShell());
		dia.open();
	}

	public void selectionChanged(IAction action, ISelection selection) {
	}

}
