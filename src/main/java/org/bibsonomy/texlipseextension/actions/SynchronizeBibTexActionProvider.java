package org.bibsonomy.texlipseextension.actions;

import net.sourceforge.texlipse.extension.BibOutlineActionProvider;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.TreeViewer;

/**
 * class for the texlipse extension point to add entries in the bib-outline page
 * 
 * @author Manuel
 *
 */
public class SynchronizeBibTexActionProvider implements BibOutlineActionProvider {
	
	@Override
	public Action getAction(final TreeViewer treeView, final IResource res) {
		return new SynchronizeWithBibSonomyAction(treeView, res);
	}

}
