package org.bibsonomy.texlipseextension.actions;

import net.sourceforge.texlipse.extension.BibOutlineActionProvider;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.TreeViewer;

/**
 * @author Manuel
 *
 */
public class AddEditBibTexProvider implements BibOutlineActionProvider {

	protected TreeViewer treeView;
	protected IResource resource;
	
	@Override
	public Action getAction(final TreeViewer treeView, final IResource res) {
		this.treeView = treeView;
		this.resource = res;
		return new AddEditBibTex(treeView, this.resource);
	}

}
