package org.bibsonomy.texlipseextension.actions;

import java.util.HashSet;
import java.util.Set;

import net.sourceforge.texlipse.TexlipsePlugin;
import net.sourceforge.texlipse.model.ReferenceEntry;

import org.bibsonomy.texlipseextension.TexLipseBibSonomyExtension;
import org.bibsonomy.texlipseextension.Bibsonomyconnection.BibFileInteractions;
import org.bibsonomy.texlipseextension.dialog.SynchronizeDialog;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchWindow;

/**
 * Represents the action button in the bib-outline page.
 * 
 * @author Manuel
 * 
 */
public class SynchronizeWithBibSonomyAction extends Action {

	/** The content tree of the outline page. */
	private final TreeViewer treeView;
	
	/** The currently opened file. */
	private final IResource res;

	public SynchronizeWithBibSonomyAction(final TreeViewer treeView, final IResource res) {
		super();
		this.treeView = treeView;
		this.res = res;
		this.setToolTipText("Synchronize with BibSonomy"); // TODO: i18n
		this.setImageDescriptor(TexLipseBibSonomyExtension.getImageDescriptor("bibSonomy"));
	}

	@Override
	public void run() {
		final TreeItem[] ti = this.treeView.getTree().getSelection();
		if (ti != null) {
			final Set<String> keys = new HashSet<String>();
			for (final Item i : ti) {
				final Object selectedElement = i.getData();
				if (selectedElement instanceof ReferenceEntry) {
					final ReferenceEntry re = (ReferenceEntry) selectedElement;
					keys.add(re.key);
				}
			}
			if (keys.size() > 0) {
				final IWorkbenchWindow window = TexlipsePlugin.getDefault().getWorkbench().getActiveWorkbenchWindow();
				// atm we only use the first selected entry, so multi-selection
				// in the tree has no effect
				// TODO enable the handling of more than one selection
				String bibKey = keys.iterator().next();

				// if we use the sort function of the outline page there is a
				// wrong bibKey, so we extract the right key
				// (...; bibKey) returned
				if (bibKey.contains("; ")) {
					bibKey = bibKey.split("; ")[1];
				}

				// the set will be used in future for more than one
				// selected entry
				final Set<String> bibKeys = new HashSet<String>();
				bibKeys.add(bibKey);

				BibFileInteractions.initSingleton((IFile) this.res);
				if (TexLipseBibSonomyExtension.getDefault().getPreferenceStore().getBoolean(TexLipseBibSonomyExtension.PREF_KEY_ENABLED)) {					
					// entry doesn't exist on server, upload possible
					if (!BibFileInteractions.getInstance().existsOnServer(bibKey)) {
						final InputDialog input = new InputDialog(window.getShell(),
								"Upload in BibSonomy",
								"Please specify the tags for the entry: "
										+ bibKey, "", null) {
							@Override
							protected void buttonPressed(final int buttonId) {
								super.buttonPressed(buttonId);
								if (buttonId == 0) {
									BibFileInteractions.getInstance()
											.addEntryToBibSonomy(
													bibKeys.iterator().next(),
													this.getValue());
								}
							}
						};
						input.open();
						// different entries, ask user what to do
					} else if (BibFileInteractions.getInstance().keyHasDuplicateWithChanges(bibKey)) {
						final SynchronizeDialog syncDia = new SynchronizeDialog(window.getShell(), bibKey);
						syncDia.open();
					} else {
						//no differences between the two entries
						MessageDialog.openInformation(window.getShell(),
										"No changes",
										"There are no differences between the local and the online entry.");
					}
				}
			}
		}
	}
}
