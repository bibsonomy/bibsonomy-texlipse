package org.bibsonomy.texlipseextension.actions;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.HashSet;
import java.util.Set;

import net.sourceforge.texlipse.TexlipsePlugin;
import net.sourceforge.texlipse.model.ReferenceEntry;

import org.bibsonomy.texlipseextension.TexLipseBibSonomyExtension;
import org.bibsonomy.texlipseextension.Bibsonomyconnection.BibSonomyConnector;
import org.bibsonomy.texlipseextension.dialog.AddEditBibTexDialog;
import org.bibsonomy.texlipseextension.model.BibFileRepresentation;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchWindow;

/**
 * @author Manuel
 *
 */
public class AddEditBibTex extends Action {

	private static final String BIBTEX_KEY_SPLIT = "; ";

	/** The content tree of the outline page. */
	private final TreeViewer treeView;
	
	/** The currently opened file. */
	private final IResource res; // TODO: unused remove?

	public AddEditBibTex(final TreeViewer treeView, final IResource res) {
		super();
		this.treeView = treeView;
		this.res = res;
		this.setToolTipText("Add/Edit BibTex");
		this.setImageDescriptor(TexLipseBibSonomyExtension.getImageDescriptor("bibSonomyTags"));
	}

	@Override
	public void run() {
		final TreeItem[] ti = this.treeView.getTree().getSelection();
		if (present(ti)) {
			final Set<String> keys = new HashSet<String>();
			for (final Item i : ti) {
				final Object selectedElement = i.getData();
				if (selectedElement instanceof ReferenceEntry) {
					final ReferenceEntry re = (ReferenceEntry) selectedElement;
					keys.add(re.key);
				}
			}
			if (present(keys)) {
				final IWorkbenchWindow window = TexlipsePlugin.getDefault().getWorkbench().getActiveWorkbenchWindow();
				/*
				 * atm we only use the first selected entry, so multi-selection
				 * in the tree has no effect
				 * TODO enable the handling of more than one selection
				 */
				String bibKey = keys.iterator().next();

				/*
				 * if we use the sort function of the outline page there is a
				 * wrong bibKey, so we extract the right key
				 * (...; bibKey) returned
				 */
				if (bibKey.contains(BIBTEX_KEY_SPLIT)) {
					bibKey = bibKey.split(BIBTEX_KEY_SPLIT)[1];
				}
				
				final BibFileRepresentation fileRep = new BibFileRepresentation(BibSonomyConnector.getCurrentFile(TexlipsePlugin.getCurrentProject()));
				
				if (TexLipseBibSonomyExtension.getDefault().getPreferenceStore().getBoolean(TexLipseBibSonomyExtension.PREF_KEY_ENABLED)) {
					final AddEditBibTexDialog dia = new AddEditBibTexDialog(window.getShell(), fileRep.getBibData(bibKey));
					dia.open();
				}
			}
		}
	}
}