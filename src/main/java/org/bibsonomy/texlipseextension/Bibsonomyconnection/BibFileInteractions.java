package org.bibsonomy.texlipseextension.Bibsonomyconnection;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bibsonomy.bibtex.parser.PostBibTeXParser;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.Tag;
import org.bibsonomy.model.User;
import org.bibsonomy.model.util.SimHash;
import org.bibsonomy.rest.client.Bibsonomy;
import org.bibsonomy.rest.client.exception.ErrorPerformingRequestException;
import org.bibsonomy.rest.client.queries.get.GetPostDetailsQuery;
import org.bibsonomy.rest.client.queries.post.CreatePostQuery;
import org.bibsonomy.rest.client.queries.put.ChangePostQuery;
import org.bibsonomy.texlipseextension.TexLipseBibSonomyExtension;
import org.bibsonomy.texlipseextension.model.BibFileEntryData;
import org.bibsonomy.texlipseextension.model.BibFileRepresentation;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.preference.IPreferenceStore;

import bibtex.parser.ParseException;

/**
 * FIXME: merge all api interactions into one class
 * 
 * Class for the .bib file outline page functions
 * 
 * @author Manuel
 * 
 */
public class BibFileInteractions {

	private static BibFileInteractions instance;

	/** representation of the connected file */
	private final BibFileRepresentation fileRep;

	/** the last checked post from the server, used to skip a second server access*/
	private Post<?> lastPost;

	private BibFileInteractions(final IFile f) {
		this.fileRep = new BibFileRepresentation(f);
	}
	
	private static String getUserName() {
		final IPreferenceStore preferenceStore = TexLipseBibSonomyExtension.getDefault().getPreferenceStore();
		return preferenceStore.getString(TexLipseBibSonomyExtension.PREF_KEY_USERNAME);
	}
	
	private static String getAPIKey() {
		final IPreferenceStore preferenceStore = TexLipseBibSonomyExtension.getDefault().getPreferenceStore();
		return preferenceStore.getString(TexLipseBibSonomyExtension.PREF_KEY_APIKEY);
	}
	
	private static String getAPIUrl() {
		final IPreferenceStore preferenceStore = TexLipseBibSonomyExtension.getDefault().getPreferenceStore();
		return preferenceStore.getString(TexLipseBibSonomyExtension.PREF_KEY_API_URL);
	}

	/**
	 * initializes the singleton with a .bib file
	 * 
	 * @param f
	 */
	public static void initSingleton(final IFile f) {
		instance = new BibFileInteractions(f);
	}
	
	/**
	 * 
	 * @return the instance
	 * @throws IllegalArgumentException
	 *             there is no file set (use initSingleton(File f) first)
	 */
	public static BibFileInteractions getInstance()
			throws IllegalArgumentException {
		if (instance == null) {
			throw new IllegalArgumentException("No BibFile set");
		}
		return instance;
	}

	/**
	 * Used to determine if there are differences on the BibTex entry from the
	 * server and the BibTex entry from the file
	 * 
	 * @param bibTexKey the key of the entry
	 * @return true if not equal
	 */
	public boolean keyHasDuplicateWithChanges(final String bibTexKey) {
		final BibFileEntryData entryData = this.fileRep.getEntries().get(bibTexKey);
		String hash = entryData.getHash();
		if (hash.equals("")) {
			hash = SimHash.getSimHash2(entryData.getBibTex());
		}
		Resource res;
		try {
			if (((BibTex) this.lastPost.getResource()).getBibtexKey().equals(
					bibTexKey)) {
				res = this.lastPost.getResource();
			} else {
				res = this.getPostForHash(hash).getResource();
			}
		} catch (final NullPointerException e) {
			// lastPost not initialized
			res = this.getPostForHash(hash).getResource();
		}

		if (res == null) {
			// there exists no Post with this key
			return false;
		}
		try {
			// check if there are differences between the entries
			return (!BibSonomyUtils.equalEntries((BibTex) res, entryData.getBibTex()));
		} catch (final ParseException e) {
//			String message = "Parsing the BibTex-data with key " + bibTexKey
//					+ " from file " + fileRep.getFile().getAbsolutePath()
//					+ " doesn't succeed!";
//			TexlipsePlugin.log(message, e);
		} catch (final IOException e) {
//			TexlipsePlugin.log("IO-Error on parsing the BibTex-data with key "
//					+ bibTexKey + " from file "
//					+ fileRep.getFile().getAbsolutePath() + "!", e);
		}
		return false;
	}

	/**
	 * Figures out if there is an entry with the same bibTex key on the
	 * BibSonomy server, if the entry has no intrahash the method creates the
	 * intrahash with the SimHash-class
	 * 
	 * @param bibTexKey
	 * @return false if there is no entry with the given hash and key on the
	 *         BibSonomy/PUMA instance.
	 */
	public boolean existsOnServer(final String bibTexKey) {
		final BibFileEntryData entryData = this.fileRep.getEntries().get(bibTexKey);
		Post<?> post = null;
		String hash = entryData.getHash();
		if (!present(hash)) {
			hash = SimHash.getSimHash2(entryData.getBibTex());
		}
		post = this.getPostForHash(hash);
		// saves this post so we don't have to fetch it again from the server
		this.lastPost = post;
		if (!present(post)) {
			return false;
		}
		return true;
	}

	/**
	 * Updates an entry on the BibSonomy/PUMA instance with the local changes.
	 * 
	 * @param bibTexKey
	 *            the key of the entry to update
	 * @return true if success
	 */
	public boolean updateEntryInBibSonomy(final String bibTexKey) {
		try {
			final Bibsonomy bib = new Bibsonomy(getUserName(), getAPIKey());
			bib.setApiURL(getAPIUrl());
			// create a post with the data from the file entry
			final String data = this.fileRep.getBibData(bibTexKey);
			String hash = this.fileRep.getEntries().get(bibTexKey).getHash();
			if (hash.equals("")) {
				hash = SimHash.getSimHash2(this.fileRep.getEntries().get(bibTexKey)
						.getBibTex());
			}
			final PostBibTeXParser parser = new PostBibTeXParser();
			final Post<?> post = parser.parseBibTeXPost(data);
			// used to get the old tags of the entry that
			// should be replaced
			Post<?> entryFromBibSonomy;
			try {
				if (((BibTex) this.lastPost.getResource()).getBibtexKey().equals(
						bibTexKey)) {
					entryFromBibSonomy = this.lastPost;
				} else {
					entryFromBibSonomy = this.getPostForHash(hash);
				}
			} catch (final NullPointerException e) {
				entryFromBibSonomy = this.getPostForHash(hash);
			}
			post.getResource().setIntraHash(hash);
			post.setTags(entryFromBibSonomy.getTags());
			post.setUser(new User(getUserName()));
			final ChangePostQuery qu = new ChangePostQuery(getUserName(),
					entryFromBibSonomy.getResource().getIntraHash(), post);
			bib.executeQuery(qu);
			this.lastPost = null;
			if (qu.getHttpStatusCode() == 200) {
				return true;
			}
		} catch (final ParseException e) {
//			String message = "Parsing the BibTex-data with key " + bibTexKey
//					+ " from file " + fileRep.getFile().getAbsolutePath()
//					+ " doesn't succeed!";
//			TexlipsePlugin.log(message, e);
		} catch (final IllegalArgumentException e) {
//			TexlipsePlugin.log("The entry with the BibTex-key " + bibTexKey
//					+ "doesn't exist on the BibSonomy server!", e);
		} catch (final IOException e) {
//			TexlipsePlugin.log("IO-Error on parsing the BibTex-data with key "
//					+ bibTexKey + " from file "
//					+ fileRep.getFile().getAbsolutePath() + "!", e);
		} catch (final IllegalStateException e) {
//			TexlipsePlugin.log("", e);
		} catch (final ErrorPerformingRequestException e) {
//			String message = "No connection to the BibSonomy-server!";
//			TexlipsePlugin.log(message, e);
		}
		return false;
	}

	/**
	 * Replaces an entry in the file with the corresponding entry of the server.
	 * 
	 * @param key the BibTex key of the entries to update
	 * @return true if success
	 */
	public boolean updateFile(final String key) {
		final List<BibTex> filesToUpdate = new ArrayList<BibTex>();
		String hash = this.fileRep.getEntries().get(key).getHash();
		if (hash.equals("")) {
			hash = SimHash.getSimHash2(this.fileRep.getEntries().get(key)
					.getBibTex());
		}
		Post<?> post;
		try {
			if (((BibTex) this.lastPost.getResource()).getBibtexKey().equals(key)) {
				post = this.lastPost;
			} else {
				post = this.getPostForHash(hash);
			}
		} catch (final NullPointerException e) {
			post = this.getPostForHash(hash);
		}
		if (post.getResource() != null) {
			filesToUpdate.add((BibTex) post.getResource());
		}
		this.lastPost = null;
		return this.fileRep.updateEntriesInFile(filesToUpdate);
	}

	/**
	 * Adds a BibTex entry to the BibSonomy system.
	 * 
	 * @param bibTexKey
	 *            the key of the entry to add
	 * @param tagsString
	 *            the tags to set for the entry
	 * @return true if success
	 */
	public boolean addEntryToBibSonomy(final String bibTexKey, final String tagsString) {
		// TODO: use restlogic interface move to common place 
		final Bibsonomy bib = new Bibsonomy(getUserName(), getAPIKey());
		bib.setApiURL(getAPIUrl());
		final PostBibTeXParser parser = new PostBibTeXParser();
		final List<String> tagsAsString = BibSonomyUtils.getTagsFromString(tagsString);
		final Set<Tag> tags = new HashSet<Tag>();
		for (final String k : tagsAsString) {
			tags.add(new Tag(k));
		}
		Post<?> post;
		try {
			final String data = this.fileRep.getBibData(bibTexKey);
			post = parser.parseBibTeXPost(data);
			post.setTags(tags);
			post.setUser(new User(getUserName()));
			final CreatePostQuery query = new CreatePostQuery(getUserName(), post);
			bib.executeQuery(query);
			if (query.getHttpStatusCode() == 200) {
				return true;
			}
		} catch (final ParseException e) {
//			String message = "Parsing the BibTex-data with key " + bibTexKey
//					+ " from file " + fileRep.getFile().getAbsolutePath()
//					+ " doesn't succeed!";
			//log message
		} catch (final IOException e) {
//			String message = "IO-Error on parsing the BibTex-data with key "
//				+ bibTexKey + " from file "
//				+ fileRep.getFile().getAbsolutePath() + "!";
			//log message
		} catch (final IllegalStateException e) {
			//log message
		} catch (final ErrorPerformingRequestException e) {
			//log message
		}
		return false;
	}

	/**
	 * Fetches an entry with the given key from the server.
	 *  
	 * @param key of the entry to fetch
	 * @return the entry as {@link BibTex} or null if not existent.
	 */
	public BibTex getEntryFromBibSonomy(final String key) {
		final BibFileEntryData entryData = this.fileRep.getEntries().get(key);
		String hash = entryData.getHash();
		if (entryData.getHash().equals("")) {
			hash = SimHash.getSimHash2(entryData.getBibTex());
		}
		Resource res;
		try {
			if (((BibTex) this.lastPost.getResource()).getBibtexKey().equals(key)) {
				res = this.lastPost.getResource();
			} else {
				res = this.getPostForHash(hash).getResource();
			}
		} catch (final NullPointerException e) {
			res = this.getPostForHash(hash).getResource();
		}
		if (res != null) {
			return (BibTex) res;
		}
		return null;
	}

	/**
	 * Returns a BibTex entry from the file as BibTex object.
	 * 
	 * @param bibTexKey
	 *            the key of the entry to fetch
	 * @return the entry as {@link BibTex} or null if not existent.
	 */
	public BibTex getEntryFromFile(final String bibTexKey) {
		final String bibTex = this.fileRep.getBibData(bibTexKey);
		final PostBibTeXParser parser = new PostBibTeXParser();
		Post<?> post;
		try {
			post = parser.parseBibTeXPost(bibTex);
			if (post.getResource() != null) {
				return (BibTex) post.getResource();
			}
		} catch (final ParseException e) {
//			String message = "Parsing the BibTex-data with key " + bibTexKey
//					+ " from file " + fileRep.getFile().getAbsolutePath()
//					+ " doesn't succeed!";
//			TexlipsePlugin.log(message, e);
		} catch (final IOException e) {
//			TexlipsePlugin.log("IO-Error on parsing the BibTex-data with key "
//					+ bibTexKey + " from file "
//					+ fileRep.getFile().getAbsolutePath() + "!", e);
		}
		return null;
	}

	/**
	 * Fetches the Post that belongs to the given hash from the bibSonomy server
	 * 
	 * @param hash
	 *            the hash for the post on the BibSonomy/PUMA instance.
	 * @return the Post or null if there exists no Post with the given hash
	 */
	private static Post<?> getPostForHash(final String hash) {
		// TODO: use restlogic interface move to common place to config api url
		final Bibsonomy bib = new Bibsonomy(getUserName(), getAPIKey());
		bib.setApiURL(getAPIUrl());
		final GetPostDetailsQuery qu = new GetPostDetailsQuery(getUserName(), hash);
		try {
			bib.executeQuery(qu);
			if (qu.getHttpStatusCode() == 200) {
				return qu.getResult();
			}
		} catch (final IllegalStateException e) {
			// TexlipsePlugin.log("", e);
		} catch (final ErrorPerformingRequestException e) {
			// String message =
			// "Error on the connection to the BibSonomy-server!";
			// TexlipsePlugin.log(message, e);
		} catch (final IllegalArgumentException e) {
			// TexlipsePlugin.log("", e);
		}
		return null;
	}

}
