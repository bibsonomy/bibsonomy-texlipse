package org.bibsonomy.texlipseextension.Bibsonomyconnection;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sourceforge.texlipse.TexlipsePlugin;
import net.sourceforge.texlipse.model.ReferenceEntry;
import net.sourceforge.texlipse.properties.TexlipseProperties;

import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.rest.client.Bibsonomy;
import org.bibsonomy.rest.client.exception.ErrorPerformingRequestException;
import org.bibsonomy.rest.client.queries.get.GetPostsQuery;
import org.bibsonomy.texlipseextension.TexLipseBibSonomyExtension;
import org.bibsonomy.texlipseextension.model.BibFileRepresentation;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.preference.IPreferenceStore;


/**
 * Main class that handles all exchange with the BibSonomy server
 * 
 * @author Manuel
 * 
 */
public class BibSonomyConnector {

	/** the userName that belongs to the bibSonomy account to work with */
	private String userName;

	/** the api Key that belongs to the bibSonoomy account to work with */
	private String apiKey;
	
	private String apiUrl;

	private boolean validUserApiCombination;

	/** the singleton instance */
	private static BibSonomyConnector instance;

	/** a Map containing the bibTex Posts as values and the bibTex keys as key */
	private Map<String, Post<?>> bibEntrys;

	/** a Map containing the bibTex keys as values and the bibTex Hashes as key */
	private Map<String, List<String>> hashes;

	/** a list containing possible tags to limit the bibTex entries to fetch */
	private List<String> tags;

	/**
	 * creates the instance
	 * 
	 * @param userName
	 *            the name that belongs to the bibSonomy account
	 */
	private BibSonomyConnector() {
		this.refreshAllData();
	}

	/**
	 * returns the singleton instance
	 * 
	 * @return instance of BibsonomyHandler
	 */
	public static BibSonomyConnector getInstance() {
		if (instance == null) {
			instance = new BibSonomyConnector();
			return instance;
		} else {
			return instance;
		}
	}

	public BibFileRepresentation getBibFileRepresentation(final IProject project) {
		return new BibFileRepresentation(getCurrentFile(project));
	}

	/**
	 * Returns the current file to store the bibTex entries
	 * 
	 * @return the file on which we are working
	 */
	public static IFile getCurrentFile(final IProject project) {
		final String[] bibFiles = (String[]) TexlipseProperties.getSessionProperty(TexlipsePlugin.getCurrentProject(), TexlipseProperties.BIBFILE_PROPERTY);
		
		// TODO: support multiple files
		if (present(bibFiles.length)) {
			return TexLipseBibSonomyExtension.getFile(project, bibFiles[0]);
		}
		// FIXME: add new reference file + add it as reference file to main latex file
		return null;
	}

	/**
	 * Refreshes all content of the handler with the actual user name, tags and
	 * api key.
	 */
	public void refreshAllData() {
		final IPreferenceStore preferenceStore = TexLipseBibSonomyExtension.getDefault().getPreferenceStore();
		this.userName = preferenceStore.getString(TexLipseBibSonomyExtension.PREF_KEY_USERNAME);
		this.tags = BibSonomyUtils.getTagsFromString(preferenceStore.getString(TexLipseBibSonomyExtension.PREF_KEY_TAGS));
		this.apiKey = preferenceStore.getString(TexLipseBibSonomyExtension.PREF_KEY_APIKEY);
		this.apiUrl = preferenceStore.getString(TexLipseBibSonomyExtension.PREF_KEY_API_URL);
		this.refreshEntries();
	}

	/**
	 * Creates and returns a list with all bibSonomy entries in the form of
	 * {@link ReferenceEntry}s (ReferenceEntries are needed for the auto-completion
	 * manager from TexLipse)
	 * 
	 * @return the list containing the bibSonomy bibTex entries as reference
	 *         entries
	 */
	public List<ReferenceEntry> getRefEntries() {
		final List<ReferenceEntry> bibEntriesList = new ArrayList<ReferenceEntry>();
		if (this.bibEntrys.size() > 0) {
			for (final String key : this.hashes.keySet()) {
				bibEntriesList.add(new ReferenceEntry(key, BibTexUtils
						.toBibtexString((BibTex) this.bibEntrys.get(
								this.hashes.get(key).get(0)).getResource())));
			}
		}
		return bibEntriesList;
	}

	/**
	 * Saves a publication entry in the file that is currently set
	 * 
	 * @param hash
	 *            the hash of the bibTex entry
	 * @return <code>true</code> iff entry was saved to file
	 */
	public boolean saveToFile(final String hash, final IProject pro) {
		final IFile file = getCurrentFile(pro); 
		final BibFileRepresentation fileRepresentation = new BibFileRepresentation(file);
		final BibTex publication = (BibTex) this.bibEntrys.get(hash).getResource();
		
//		// TODO: add a timestamp; use bibsonomy's date fields
//		Date date = GregorianCalendar.getInstance().getTime();
//		SimpleDateFormat format = new SimpleDateFormat();
//		String d = format.format(date);
//		if (!bib.getMisc().equals(""))
//		bib.setMisc(bib.getMisc() + ", timestamp={" + d+"}");
//		else bib.setMisc("timestamp={" + d+"}");
		
		publication.parseMiscField();
		return fileRepresentation.writeEntryToFile(publication);
	}

	public List<Post<?>> getHashesForKey(final String key) {
		final List<Post<?>> back = new ArrayList<Post<?>>();
		for (final String hash : this.hashes.get(key)) {
			back.add(this.bibEntrys.get(hash));
		}
		return back;
	}

	/**
	 * Refreshes the entries by re-fetching them from the bibSonomy server atm
	 * limited to 10000 entries
	 */
	private void refreshEntries() {
		this.bibEntrys = new HashMap<String, Post<?>>();
		this.hashes = new HashMap<String, List<String>>();
		if (!TexLipseBibSonomyExtension.getDefault().getPreferenceStore().getBoolean(TexLipseBibSonomyExtension.PREF_KEY_ENABLED)) {
			return;
		}
		try {
			final Bibsonomy bib = new Bibsonomy(this.userName, this.apiKey);
			bib.setApiURL(this.apiUrl);
			final GetPostsQuery getPostsQuery = new GetPostsQuery(0, 1000);
			getPostsQuery.setResourceType(BibTex.class);
			getPostsQuery.setGrouping(GroupingEntity.USER, this.userName);
			getPostsQuery.setTags(this.tags);
			try {
				bib.executeQuery(getPostsQuery);
				if (getPostsQuery.getHttpStatusCode() == 200) {
					this.validUserApiCombination = true;
					for (final Post<?> b : getPostsQuery.getResult()) {
						final BibTex k = (BibTex) b.getResource();
						this.bibEntrys.put(k.getIntraHash(), b);
						if (!this.hashes.containsKey(k.getBibtexKey())) {
							final List<String> hashList = new ArrayList<String>();
							hashList.add(k.getIntraHash());
							this.hashes.put(k.getBibtexKey(), hashList);
						} else {
							this.hashes.get(k.getBibtexKey()).add(k.getIntraHash());
						}
					}
				} else if (getPostsQuery.getHttpStatusCode() == 401) {
					this.validUserApiCombination = false;
				}
			} catch (final IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final ErrorPerformingRequestException e) {
				// no connection to BibSOnomy server, user is informed when he
				// uses the auto-completion
				 e.printStackTrace();
			}
		} catch (final IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// No username set, do nothing
		}
	}
	
	public boolean checkUsernameApiKeyValid() {
		return this.validUserApiCombination;
	}
}
