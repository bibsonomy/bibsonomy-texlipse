package org.bibsonomy.texlipseextension.Bibsonomyconnection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.bibsonomy.model.BibTex;

import bibtex.parser.ParseException;

/**
 * FIXME: use diff library
 * 
 * 
 * A class containing some methods used in the plugin.
 * 
 * @author Manuel
 *
 */
public class BibSonomyUtils {

	/**
	 * Checks whether a bibTex Object and a bibTex entry have equal fields. This method is 
	 * not very nice and will disappear in future releases (when the bibsonomy synchronize
	 * is ready).
	 * @param bib1 first BibTex entry to compare
	 * @param bib2 second BibTex entry to compare
	 * @return returns true if the regarded fields of both entries are equal
	 * @throws ParseException
	 * @throws IOException
	 */
	public static boolean equalEntries(final BibTex bib1, final BibTex bib2) throws ParseException, IOException {
		final List<String> bib1Fields = getFields(bib1);
		final List<String> bib2Fields = getFields(bib2);
		for (int i = 0; i < bib1Fields.size(); i++) {
			try {
				if (!bib1Fields.get(i).equals(bib2Fields.get(i))) {
					return false;
				}
			} catch (final NullPointerException e) {
				if (((bib1Fields.get(i) == null) && (bib2Fields.get(i) != null)) || ((bib2Fields.get(i) == null) && (bib1Fields.get(i) != null))) {
					return false;
				}
			}
		}
		return true;
	}
	
	private static List<String> getFields(final BibTex bibTex) {
		final List<String> temp = new ArrayList<String>();
		temp.add(bibTex.getBibtexKey());
		temp.add(bibTex.getAbstract());
		temp.add(bibTex.getAddress());
		temp.add(bibTex.getAnnote());
		temp.add(bibTex.getAuthor());
		temp.add(bibTex.getChapter());
		temp.add(bibTex.getBooktitle());
		temp.add(bibTex.getCrossref());
		temp.add(bibTex.getDay());
		temp.add(bibTex.getEdition());
		temp.add(bibTex.getEditor());
		temp.add(bibTex.getEntrytype());
		temp.add(bibTex.getHowpublished());
		temp.add(bibTex.getInstitution());
		temp.add(bibTex.getJournal());
		temp.add(bibTex.getMonth());
		temp.add(bibTex.getNote());
		temp.add(bibTex.getNumber());
		temp.add(bibTex.getOpenURL());
		temp.add(bibTex.getOrganization());
		temp.add(bibTex.getPages());
		temp.add(bibTex.getPrivnote());
		temp.add(bibTex.getPublisher());
		temp.add(bibTex.getSchool());
		temp.add(bibTex.getSeries());
		temp.add(bibTex.getTitle());
		temp.add(bibTex.getType());
		temp.add(bibTex.getUrl());
		temp.add(bibTex.getVolume());
		temp.add(bibTex.getYear());
		return temp;
	}
	
	private static String addLineSep(final String k, final int lenght) {
		final StringBuffer back = new StringBuffer();
		
		if (k.length() > lenght) {
			final double x = Math.ceil(k.length() / lenght);
			for (int i = 0; i<x; i++) {
				final String j = k.substring(120*i, 120*(i+1));
				back.append(j+System.getProperty("line.separator"));
			}
			back.append(k.substring((int)(120*(x)), k.length()));
		} else {
			back.append(k);
		}
		return back.toString();
	}
	
	/**
	 * splits a white-space separated String with some tags and returns a set
	 * containing this tags
	 * 
	 * @param tagsString
	 *            a string containing some white-space separated tags
	 * 
	 * @return a set containing the tags
	 */
	public static List<String> getTagsFromString(final String tagsString) {
		final Scanner sc = new Scanner(tagsString);
		final List<String> returnSet = new ArrayList<String>();
		sc.useDelimiter("[\\s]+");
		while (sc.hasNext()) {
			returnSet.add(sc.next());
		}
		return returnSet;
	}
	
	/**
	 * creates a {@link LinkedHashSet} containing the trimmed lines of a string. The
	 * {@link LinkedHashSet} is needed to keep the order of the lines. If the String is a BibTexentry
	 * the hashes are ignored.
	 * 
	 * @param s
	 * @return
	 */
	public static LinkedHashSet<String> getLinesAsSet(String s) {
		final LinkedHashSet<String> back = new LinkedHashSet<String>();
		s = s.trim();
		
		//normalize the different line.separators from the different operating systems
		s = s.replace("\r\n", "\n");
		s = s.replace("\r", "\n");
		
		final Pattern pat = Pattern.compile("\n");
		final Pattern pat2 = Pattern.compile(".*intrahash.*|.*interhash.*",Pattern.CASE_INSENSITIVE);
		final String[] lines = s.split(pat.pattern());
		for(String l : lines) {
			//don't display the hashes
			if (l.matches(pat2.pattern())) {
				continue;
			} else if (l.length() > 120) {
				l = addLineSep(l, 120);
			}
			back.add(l);
		}
		return back;
	}
}
