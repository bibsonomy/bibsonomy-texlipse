package org.bibsonomy.texlipseextension.Bibsonomyconnection;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sourceforge.texlipse.TexlipsePlugin;
import net.sourceforge.texlipse.extension.BibProvider;
import net.sourceforge.texlipse.model.AbstractEntry;
import net.sourceforge.texlipse.model.ReferenceContainer;
import net.sourceforge.texlipse.model.ReferenceEntry;
import net.sourceforge.texlipse.properties.TexlipseProperties;

import org.bibsonomy.texlipseextension.TexLipseBibSonomyExtension;
import org.bibsonomy.texlipseextension.model.BibSonomyCompletionProposal;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.swt.graphics.Image;

/**
 * Class that belongs to the extension point that enables additional \cite
 * completion entries
 * 
 * The getCompletionsBin(*) methods and the wrapString() method are directly
 * copied from the TexlipsePlugin project, this is an artifact from the time
 * when this plug-in was directly integrated in TexLipse (perhaps there is a
 * better way to do this).
 * 
 * @author Manuel
 */
public class BibCompletionProvider implements BibProvider {

	@Override
	public List<ICompletionProposal> getCompletions(final int offset, final int replacementLength, final String prefix, final ReferenceContainer bibContainer) {
		final BibSonomyConnector datastore = BibSonomyConnector.getInstance();		
		final int assistLineLength = 60;

		final Set<String> keysBibSonomy = new HashSet<String>();
		final Set<String> keysTexLipse = new HashSet<String>();
		final Map<String, ReferenceEntry> entries = new HashMap<String, ReferenceEntry>();
		
		for (final ReferenceEntry ref : datastore.getRefEntries()) {
			entries.put(ref.key, (ReferenceEntry)ref.copy());
			keysBibSonomy.add(ref.key);
		}
		for (final ReferenceEntry ref : bibContainer.getSortedReferences()) {
			entries.put(ref.key, (ReferenceEntry)ref.copy());
			keysTexLipse.add(ref.key);
		}
		
		if (!present(BibSonomyConnector.getCurrentFile(TexlipsePlugin.getCurrentProject()))) {
			return null;
		}	
		//HashSet<String> duplicates = new HashSet<String>();	
//		List<ReferenceEntry> entriesFromFile = new ArrayList<ReferenceEntry>();
//	//	if (bibContainer.getSortedReferences().size() == 0) {
//			
//			BibFileRepresentation fileRep = new BibFileRepresentation(bibH.getCurrentFile());
//			Map<String, BibFileEntryData> entries = fileRep.getEntries();
//			for (String key : entries.keySet()) {
//				entriesFromFile.add(new ReferenceEntry(key, BibTexUtils
//						.toBibtexString((BibTex)entries.get(key).getBibTex())));
//				
//			}
////			entriesFromBibSonomy.addAll(entriesFromFile);
////		}
//		bibContainer.addRefSource("Bibsonomy",
//					entriesFromFile);
		
		
//		bibContainer.addRefSource(bibH.getCurrentFile().getName(),
//				new ArrayList<ReferenceEntry>(entries.values()));
		final ReferenceContainer ref = new ReferenceContainer();
		
		final String[] bibFiles = (String[]) TexlipseProperties.getSessionProperty(
				TexlipsePlugin.getCurrentProject(),
				TexlipseProperties.BIBFILE_PROPERTY);
		String refSource ="BibSonomy";
		if (bibFiles.length >0) {
			refSource = bibFiles[0];
		}
		
		ref.addRefSource(refSource,new ArrayList<ReferenceEntry>(entries.values()));
		ref.organize();
		
//		bibContainer.addRefSource("Bibsonomy",entriesFromBibSonomy);
//		bibContainer.organize();
		final List<ReferenceEntry> bibEntries = this.getCompletionsBib(ref,
				prefix);
		
//		List<ReferenceEntry> bibEntries2 = new ArrayList<ReferenceEntry>();
//		bibEntries.addAll(bibContainer.getSortedReferences());
//		bibEntries.addAll(entriesFromBibSonomy);
		
		final Image im = TexLipseBibSonomyExtension.getImage("bibSonomy");
		final Image inSyncIm = TexLipseBibSonomyExtension.getImage("bibSonomyinSync");
		
		if (bibEntries != null) {
			final Map<String, ICompletionProposal> result = new LinkedHashMap<String, ICompletionProposal>();
			for (int i = 0; i < bibEntries.size(); i++) {
				final ReferenceEntry bib = bibEntries.get(i);
				final String infoText = bib.info.length() > assistLineLength ? wrapString(bib.info, assistLineLength) : bib.info;	
				if (keysBibSonomy.contains(bib.key) && keysTexLipse.contains(bib.key) ) {
					result.put(bib.key, new CompletionProposal(bib.key, offset - replacementLength, replacementLength, bib.key.length(), inSyncIm, bib.key, null, infoText));
				} else if (!keysBibSonomy.contains(bib.key)) {
					result.put(bib.key, new CompletionProposal(bib.key, offset - replacementLength, replacementLength, bib.key.length(), null, bib.key, null, infoText));
				} else {
					result.put(bib.key, new BibSonomyCompletionProposal(offset, im, bib.key, null, bib.info, replacementLength, bib, bibContainer));
				}
			}
			return new ArrayList<ICompletionProposal>(result.values());
		}
		// no entries existent
		return null;
	}
	
	/**
	 * Wraps the given string to the given column width.
	 * 
	 * @param input
	 *            The string to wrap
	 * @param width
	 *            The wrapping width
	 * @return The wrapped string
	 */
	public static String wrapString(final String input, final int width) {
		final StringBuffer sbout = new StringBuffer();

		// \n should suffice since we prettify in parsing...
		final String[] paragraphs = input.split("\r\n|\n|\r");
		for (final String paragraph : paragraphs) {
			// skip if short
			if (paragraph.length() < width) {
				sbout.append(paragraph);
				sbout.append("\n");
				continue;
			}
			// imagine how much better this would be with functional
			// programming...
			final String[] words = paragraph.split("\\s");
			int currLength = 0;
			for (final String word : words) {
				if (((word.length() + currLength) <= width) || (currLength == 0)) {
					if (currLength > 0) {
						sbout.append(" ");
					}
					sbout.append(word);
					currLength += 1 + word.length();
				} else {
					sbout.append("\n");
					sbout.append(word);
					currLength = word.length();
				}
			}
			sbout.append("\n");
		}
		return sbout.toString();
	}

	public List<ReferenceEntry> getCompletionsBib(
			final ReferenceContainer bibContainer, final String start) {
		final List<ReferenceEntry> bibEntries = bibContainer.getSortedReferences();

		if (bibEntries == null) {
			return null;
		}
		if (start.equals("")) {
			return bibEntries;
		}

		// don't refetch the proposal list in partial fill;
		// use the existing proposal list and make it smaller
		int[] bounds;
		// if (lastBib.length() > 0 && start.startsWith(lastBib))
		// bounds = getCompletionsBin(start, bibEntries, lastBibBounds);
		// else
		// bounds = getCompletionsBin(start, bibEntries);

		// ...either solve problems with bounds or remove them...
		bounds = this.getCompletionsBin(start, bibEntries, true);

		if (bounds[0] == -1) {
			return null;
		}
		return bibEntries.subList(bounds[0], bounds[1]);
	}

	protected int[] getCompletionsBin(final String start, final List<? extends AbstractEntry> entries, final boolean lowerCase) {
		return this.getCompletionsBin(start, entries, new int[] { 0, entries.size() }, lowerCase);
	}

	protected int[] getCompletionsBin(final String start, final AbstractEntry[] entries) {
		return this.getCompletionsBin(start, Arrays.asList(entries), new int[] { 0, entries.length }, false);
	}

	protected int[] getCompletionsBin(String start, final List<? extends AbstractEntry> entries, final int[] initBounds, final boolean lowerCase) {
		final int[] bounds = new int[] { -1, -1 };
		int left = initBounds[0], right = initBounds[1] - 1;
		int middle = right / 2;
		if (left > right) {
			return bounds;
		}
		if (lowerCase) {
			start = start.toLowerCase();
		}

		if (entries.get(left).getkey(lowerCase).startsWith(start)) {
			right = middle = left;
		}

		// get upper bound (inclusive)
		while (left < middle) {
			if (entries.get(middle).getkey(lowerCase).compareTo(start) >= 0) {
				right = middle;
				middle = (left + middle) / 2;
			} else {
				left = middle;
				middle = (middle + right) / 2;
			}
		}
		if (!entries.get(right).getkey(lowerCase).startsWith(start)) {
			return bounds;
		}

		bounds[0] = right;

		// get lower bound (exclusive)
		left = right;
		right = initBounds[1] - 1;

		if (entries.get(right).getkey(lowerCase).startsWith(start)) {
			bounds[1] = right + 1;
			return bounds;
		}
		middle = (left + right) / 2;
		while (left < middle) {
			if (entries.get(middle).getkey(lowerCase).startsWith(start)) {
				left = middle;
				middle = (right + middle) / 2;
			} else {
				right = middle;
				middle = (middle + left) / 2;
			}
		}
		bounds[1] = right;
		return bounds;
	}
}
