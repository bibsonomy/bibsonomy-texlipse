package org.bibsonomy.texlipseextension.dialog;

import org.bibsonomy.texlipseextension.TexLipseBibSonomyExtension;
import org.bibsonomy.texlipseextension.Bibsonomyconnection.BibSonomyConnector;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Dialog that pop-ups when the change-tags button is hit
 * 
 * @author Manuel
 *
 */
public class TagDialog extends Dialog {

	private Text tags;
	
	public TagDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {		
		Composite composite = (Composite) super.createDialogArea(parent);
		tags = new Text(composite, SWT.SINGLE | SWT.BORDER);
		tags.setText(TexLipseBibSonomyExtension.getDefault().getPreferenceStore().getString(TexLipseBibSonomyExtension.PREF_KEY_TAGS));
		tags.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
                | GridData.HORIZONTAL_ALIGN_FILL));
		return composite;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, 0, "Set tags", true);
		createButton(parent, 1, "Refresh entries", false);
		createButton(parent, 2, "Cancel", false);
	}
	
	protected void buttonPressed(int buttonId) {
        if (buttonId == 0) {
            TexLipseBibSonomyExtension.getDefault().getPreferenceStore()
    		.setValue(TexLipseBibSonomyExtension.PREF_KEY_TAGS,
    				tags.getText());
        } 
        else if (buttonId == 1) {
            BibSonomyConnector.getInstance().refreshAllData();
        }
        close();
    }		
	
}
