/**
 * 
 */
package org.bibsonomy.texlipseextension.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;



/**
 * @author Manuel
 *
 */
public class AddEditBibTexDialog extends Dialog {

	private String bibData;
	
	/**
	 * @param parentShell
	 */
	public AddEditBibTexDialog(Shell parentShell, String bibData) {
		super(parentShell);
		this.bibData = bibData;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {		
		Composite composite = (Composite) super.createDialogArea(parent);
		Text tags = new Text(composite, SWT.SINGLE | SWT.BORDER);
		tags.setText(bibData);
		tags.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
                | GridData.HORIZONTAL_ALIGN_FILL));
		return composite;
	}

}
