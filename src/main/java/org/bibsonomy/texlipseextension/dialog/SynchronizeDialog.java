package org.bibsonomy.texlipseextension.dialog;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.texlipseextension.Bibsonomyconnection.BibFileInteractions;
import org.bibsonomy.texlipseextension.Bibsonomyconnection.BibSonomyUtils;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * The dialog that shows the differences between an online and a local BibTex
 * entry.
 * 
 * @author Manuel
 * 
 */
public class SynchronizeDialog extends Dialog {

	/** the key of the corresponding entry	 */
	private final String bibKey;

	public SynchronizeDialog(final Shell shell, final String bibKey) {
		super(shell);
		this.bibKey = bibKey;
	}

	@Override
	protected Control createDialogArea(final Composite parent) {
		final Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(2, true));

		// left group
		final Group lgroup = new Group(composite, SWT.SHADOW_IN);
		lgroup.setLayout(new GridLayout());
		// right group
		final Group rgroup = new Group(composite, SWT.SHADOW_IN);
		rgroup.setLayout(new GridLayout());

		final Label serverLabel = new Label(lgroup, 1);
		serverLabel.setText("Server:");
		final Label lokalLabel = new Label(rgroup, 1);
		lokalLabel.setText("Lokal:");

		if (this.bibKey != null) {
			BibTex serverEntry = null;
			BibTex localEntry = null;
			try {
				serverEntry = BibFileInteractions.getInstance().getEntryFromBibSonomy(this.bibKey);
				localEntry = BibFileInteractions.getInstance().getEntryFromFile(this.bibKey);
			} catch (final IllegalArgumentException e) {
				//String message = "The BibTex Editor was called but no file was set!";
				//TexlipsePlugin.log(message, e);
				return null;
			}
			if ((localEntry == null) || (serverEntry == null)) {
				//String message = "Either the BibTex entry from the server or from the file doesn't exist!";
				//TexlipsePlugin.log(message, new IllegalArgumentException());
				return null;
			}
			// XXX: this is not very nice
			final LinkedHashSet<String> serverLines = BibSonomyUtils.getLinesAsSet(BibTexUtils.toBibtexString(serverEntry));
			final LinkedHashSet<String> localLines = BibSonomyUtils.getLinesAsSet(BibTexUtils.toBibtexString(localEntry));

			// a set containing the difference lines between the server and the
			// local entry
			final Set<String> differences = new HashSet<String>();
			for (final String k : serverLines) {
				if (!localLines.contains(k)) {
					differences.add(k);
				}
			}
			for (final String k : localLines) {
				if (!serverLines.contains(k)) {
					differences.add(k);
				}
			}
			
			// create the labels and mark the differences with red
			for (int i = 0; i < Math.max(serverLines.size(), localLines.size()); i++) {
				try {
					final Label labelServer = new Label(lgroup, 1);
					labelServer.setText((String) serverLines.toArray()[i]);
					if (differences.contains(serverLines.toArray()[i])) {
						labelServer.setBackground(parent.getShell()
								.getDisplay().getSystemColor(SWT.COLOR_RED));
					}
				} catch (final IndexOutOfBoundsException e) {
					// doesn't matter
					// String message =
					// "The local entry owns more lines than the server entry.";
					// TexlipsePlugin.log(message, e, IStatus.OK);
				}

				try {
					final Label labelLocal = new Label(rgroup, 1);
					labelLocal.setText((String) localLines.toArray()[i]);
					if (differences.contains(localLines.toArray()[i])) {
						labelLocal.setBackground(parent.getShell().getDisplay()
								.getSystemColor(SWT.COLOR_RED));
					}
				} catch (final IndexOutOfBoundsException e) {
					// doesn't matter
					// String message =
					// "The server entry owns more lines than the local entry.";
					// TexlipsePlugin.log(message, e, IStatus.OK);
				}
			}

		}
		return composite;
	}

	@Override
	protected void createButtonsForButtonBar(final Composite parent) {
		this.createButton(parent, 0, "Update on server", true);
		this.createButton(parent, 1, "Update in local file", false);
	}

	@Override
	protected void buttonPressed(final int buttonId) {
		// update the entry on BibSonomy server
		if (buttonId == 0) {
			try {
				BibFileInteractions.getInstance().updateEntryInBibSonomy(
						this.bibKey);
			} catch (final IllegalArgumentException e) {
//				String message = "The BibTex Editor was called but no file was set!"
//						+ System.getProperty("line.separator");
//				if (buttonId == 0) {
//					message += "The \"Update on Server\" button was hit";
//				} else {
//					message += "No button was hit";
//				}
//				TexlipsePlugin.log(message, e);
			}
		}
		// update the entry in the file
		else {
			try {
				BibFileInteractions.getInstance().updateFile(this.bibKey);
			} catch (final IllegalArgumentException e) {
//				String message = "The BibTex Editor was called but no file was set!"
//						+ System.getProperty("line.separator");
//				if (buttonId == 1) {
//					message += "The \"Update in local file\"-button was hit";
//				} else {
//					message += "No button was hit";
//				}
//				TexlipsePlugin.log(message, e);
			}
		}
		super.buttonPressed(buttonId);
	}

}
